## MPS SMSWS Emulation

This Project is a simple Emulation that help return SMSWS according to WSDL from Viettel Telecom.

In production, the following messages are exchange between Client (Content Provider - CP) and Operator (Viettel Telecom - Vietnam):

__Web Service Description Language__


~~~xml

<wsdl:definitions targetNamespace="http://smsws/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:axis2="http://smsws/" xmlns:ns1="http://org.apache.axis2/xsd" xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:ns0="http://smsws/xsd" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/">
   <wsdl:documentation>smsws</wsdl:documentation>
   <wsdl:types>
      <xs:schema attributeFormDefault="qualified" elementFormDefault="qualified" targetNamespace="http://smsws/xsd" xmlns:ns="http://smsws/xsd">
         <xs:element name="smsRequest">
            <xs:complexType>
               <xs:sequence>
                  <xs:element minOccurs="0" name="username" nillable="true" type="xs:string"/>
                  <xs:element minOccurs="0" name="password" nillable="true" type="xs:string"/>
                  <xs:element minOccurs="0" name="msisdn" nillable="true" type="xs:string"/>
                  <xs:element minOccurs="0" name="content" nillable="true" type="xs:string"/>
                  <xs:element minOccurs="0" name="shortcode" nillable="true" type="xs:string"/>
                  <xs:element minOccurs="0" name="alias" nillable="true" type="xs:string"/>
                  <xs:element minOccurs="0" name="params" nillable="true" type="xs:string"/>
               </xs:sequence>
            </xs:complexType>
         </xs:element>
         <xs:element name="smsRequestResponse">
            <xs:complexType>
               <xs:sequence>
                  <xs:element minOccurs="0" name="return" type="xs:string"/>
               </xs:sequence>
            </xs:complexType>
         </xs:element>
      </xs:schema>
   </wsdl:types>
   <wsdl:message name="smsRequestRequest">
      <wsdl:part name="parameters" element="ns0:smsRequest"/>
   </wsdl:message>
   <wsdl:message name="smsRequestResponse">
      <wsdl:part name="parameters" element="ns0:smsRequestResponse"/>
   </wsdl:message>
   <wsdl:portType name="smswsPortType">
      <wsdl:operation name="smsRequest">
         <wsdl:input message="axis2:smsRequestRequest" wsaw:Action="urn:smsRequest"/>
         <wsdl:output message="axis2:smsRequestResponse" wsaw:Action="urn:smsRequestResponse"/>
      </wsdl:operation>
   </wsdl:portType>
   <wsdl:binding name="smswsSOAP11Binding" type="axis2:smswsPortType">
      <soap:binding transport="http://schemas.xmlsoap.org/soap/http" style="document"/>
      <wsdl:operation name="smsRequest">
         <soap:operation soapAction="urn:smsRequest" style="document"/>
         <wsdl:input>
            <soap:body use="literal"/>
         </wsdl:input>
         <wsdl:output>
            <soap:body use="literal"/>
         </wsdl:output>
      </wsdl:operation>
   </wsdl:binding>
   <wsdl:service name="smsws">
      <wsdl:port name="smswsSOAP11port_http" binding="axis2:smswsSOAP11Binding">
         <soap:address location="http://10.58.128.107:8310/smsws"/>
      </wsdl:port>
   </wsdl:service>
</wsdl:definitions>

~~~

__Request__


~~~xml

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cw="http://cw.cm/">
   <soapenv:Header/>
   <soapenv:Body>
      <cw:sentMt>
         <username>jobalert</username>
         <password>jobalert@9142</password>
         <msisdn>84989999388</msisdn>
         <content>test content</content>
         <shortcode>9142</shortcode>
         <alias></alias>
         <params>text</params>
      </cw:sentMt>
   </soapenv:Body>
</soapenv:Envelope>

~~~

__Response__


~~~xml

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://smsws/xsd">
   <soapenv:Header/>
   <soapenv:Body>
      <xsd:smsRequestResponse>
         <xsd:return>0</xsd:return>
      </xsd:smsRequestResponse>
   </soapenv:Body>
</soapenv:Envelope>

~~~

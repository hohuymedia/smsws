package com.hohuy.mps.util;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class Telco {
	private Logger logger = Logger.getLogger(Telco.class);

	@Value("${telco.mcc:84}")
	public String mcc;
	
	@Value("${telco.msisdnRegexp:^84[189][0-9]{8,10}$}")
	public String msisdnRegexp;
	
	/**
	 * Provide full MSISDN format: 84977014185
	 * @param msisdn
	 * @return
	 */
	public String fullMsisdn(String msisdn){
		String isdn = msisdn;
		if (msisdn.startsWith("0")){
			isdn = mcc + msisdn.substring(1);
		}
		if (!msisdn.startsWith(mcc)){
			isdn = mcc + msisdn;
		}
		if (isdn.matches(msisdnRegexp)){	// ^84[1-9][0-9]{8,9}$
			return isdn;
		} else {
			logger.error("Found invalid MSISDN " + isdn + " convert from " + msisdn + ", MCC " + mcc + " REGEXP: " + msisdnRegexp);
		}
		return null;
	}
	
	
}

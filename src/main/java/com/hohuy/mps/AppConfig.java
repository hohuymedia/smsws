/**
 * 
 */
package com.hohuy.mps;

import java.io.Serializable;
import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author giangdd
 *
 */
@Component
@Scope("singleton")
public class AppConfig implements Serializable {
	private static final long serialVersionUID = -7605681936529899163L;
	private Logger logger = Logger.getLogger(AppConfig.class);
	private static AppConfig appConfig;

	/**
	 * Platform ShortCode
	 */
	@Value("${vtl.mps.shortcode}")
	private String smsShortcode;
	/**
	 * MT SMS sending Endpoint Configuration
	 */
	@Value("${vtl.mps.smsws.username}")
	private String smsClientUser;
	@Value("${vtl.mps.smsws.password}")
	private String smsClientPass;

	private AppConfig() {
	}

	public static AppConfig getInstance() {
		return appConfig;
	}

	@PostConstruct
	private void load() {
		logger.info("========== SHORTCODE: " + this.smsShortcode + "==============");
		logger.info("========== SMSWS: " + this.smsClientUser + "/" + this.smsClientPass + "==============");
	}


	public String getSmsClientUser() {
		return smsClientUser;
	}

	public String getSmsClientPass() {
		return smsClientPass;
	}

	public String getShortcode() {
		return smsShortcode;
	}
}

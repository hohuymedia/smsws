package com.hohuy.mps.soap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.hohuy.mps.AppConfig;
import com.hohuy.mps.soap.smsws.SmsRequest;
import com.hohuy.mps.soap.smsws.SmsRequestResponse;
import com.hohuy.mps.soap.smsws.SmswsPortType;
import com.hohuy.mps.util.Telco;

@Component
@Scope("singleton")
public class SmswsEndpoint implements SmswsPortType {
	private Logger logger = Logger.getLogger(SmswsEndpoint.class);

	@Autowired
	private AppConfig cfg;
	
	@Autowired
	private Telco util;
	
	@Override
	public String smsRequest(String username, String password, String msisdn, String content, String shortcode,
			String alias, String params) {
		// Check if this client credentials is permit to send SMS...
		if (username.equals(cfg.getSmsClientUser()) && password.equals(cfg.getSmsClientPass())){
			if (!cfg.getShortcode().equals(shortcode)){
				logger.error("Invalid short code " + shortcode + " in request. Please use this short code instead: " + cfg.getShortcode());
				return SmsRequestResponse.INVALID;
			}
			String isdn = util.fullMsisdn(msisdn);
			if (isdn == null) {
				logger.error("Failed to send SMS out due to invalid MSISDN " + msisdn);
				return SmsRequestResponse.INVALID;
			}
			switch (params.toUpperCase()){
			case SmsRequest.FLASHSMS:
				logger.info("Gotta send Flash SMS");
				break;
			case SmsRequest.HEX_FLASH:
				logger.info("Gotta send Hex Flash SMS");
				break;
			case SmsRequest.TEXT:
				logger.info("Gotta send Text SMS");
				break;
			case SmsRequest.HEX_TEXT:
				logger.info("Gotta send Hex Text SMS");
				break;
			default:
				logger.error("Invalid Params of type " + params);
				return SmsRequestResponse.WRONG_TYPE;
			}
			if (((int) (Math.random()*4)) == 1){
				logger.error("Failed to send dummy SMS! How unfortunately! SMS failed " + shortcode + " --> " + isdn + ": " + content);
				return SmsRequestResponse.FAILED;
			}
			logger.info("SMS sent: " + shortcode + " --> " + isdn + ": " + content);
			return SmsRequestResponse.SUCCESS;
		}
		logger.error("Username and Password not matched. Please send using following credential: " + cfg.getSmsClientUser() + "/" + cfg.getSmsClientPass());
		return SmsRequestResponse.WRONG_USER;
	}

}

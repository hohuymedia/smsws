/**
 * 
 */
package com.hohuy.mps.soap;

import javax.annotation.PostConstruct;
import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hohuy.mps.soap.smsws.SmswsService;
import com.hohuy.mps.soap.smsws.SmswsPortType;

/**
 * @author giangdd
 *
 */
@Configuration
public class CxfConfig {
	
	private Logger logger = Logger.getLogger(CxfConfig.class);
	
	@Autowired
	private SmswsPortType sms;
	
	@Value("${api.webroot:/ws/*}")
	private String webRoot;
	
	@Value("${api.smsws-srv:/smsws}")
	private String smswsEndpoint;
	
	@PostConstruct
	private void load() {
		logger.info("================= MPS WEB SERVICE ENDPOINTS ======================");
		logger.info("========== WEBROOT: " + this.webRoot + "==============");
		logger.info("========== --- SMSWS: " + this.smswsEndpoint + "==============");
	}
	
	@Bean
    public ServletRegistrationBean dispatcherServlet() {
        return new ServletRegistrationBean(new CXFServlet(), webRoot);
    }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }    
    
    @Bean
    public SmswsService smswsService() {
		return new SmswsService();
	}
    
    @Bean
    public Endpoint endpointSmsws() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), sms);        
        endpoint.setServiceName(smswsService().getServiceName());
        endpoint.setWsdlLocation(smswsService().getWSDLDocumentLocation().toString());
        endpoint.publish(smswsEndpoint);
        return endpoint;
    }
    
}

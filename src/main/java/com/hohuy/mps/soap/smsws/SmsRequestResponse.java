
package com.hohuy.mps.soap.smsws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "smsRequestResponse")
public class SmsRequestResponse {

    public static final String SUCCESS = "0"; 			// SMS put to queue Success
    public static final String FAILED = "1"; 			//  SMS put to Queue failed
    public static final String WRONG_WS = "2";  		// Unknow webservice
    public static final String INVALID = "200"; 		// Invalid Parameter(s)
    public static final String WRONG_USER = "201"; 		// Invalid username or password
    public static final String WRONG_TYPE = "202"; 		// Invalid Request Type
    public static final String WRONG_PARAM = "203"; 	// Invalid Request Parameter(s)
    public static final String BUSY = "400"; 			// Server too busy

    
	@XmlElement(name = "return")
    protected String _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturn(String value) {
        this._return = value;
    }

}
